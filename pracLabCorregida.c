#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <mpi.h>


//AUTOR: Edinson Quizhpe
//CICLO: 7° "A"
double operacion(int N);
int main(void)
{
    
    int N = 10000000;
    int p, np, n;
    
    double res_local, res_total;
    
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &p);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    n = N / np;
    
    if (p == 0)
    {        
        res_local = operacion(n);    
        MPI_Bcast(&res_local, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);    
        
    }
    if (p == 1)
    {
        res_local = operacion(n);    
        MPI_Bcast(&res_local, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);    
    }   
    
    MPI_Reduce(&res_local, &res_total, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    if (p == 0) {
        res_total = res_total / np;
        printf("Respuesta: %f\n", res_total);
    }
    MPI_Finalize();
    return 0;
}
double operacion(int t)
{
    int cont=0;
    double a, b;
    int i = 0;
    while (i < t)
    {
        i=i+1;
        a = rand() / (double)RAND_MAX;
        b = rand() / (double)RAND_MAX;        
        if ((a*a) + (b*b) <= 1.0)
        {
            cont += 1;
        }
    }
    return 4.0 * (double)cont / (double)t;
}